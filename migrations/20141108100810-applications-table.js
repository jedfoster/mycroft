module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.migrator.sequelize.query('ALTER TABLE "tokens" DROP CONSTRAINT IF EXISTS tokens_pkey');
    migration.migrator.sequelize.query('ALTER TABLE "tokens" ADD CONSTRAINT tokens_pkey PRIMARY KEY("token")');

    migration.createTable(
      'applications',
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        apiKey: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        apiSecret: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        name: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    );

    migration.createTable(
      'oAuth',
      {
        applicationId: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        oauthKey: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        oauthSecret: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        type: {
          type: DataTypes.STRING,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    );
   
    migration.createTable(
      'authGitHub',
      {
        userId: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        githubUserId: {
          type: DataTypes.INTEGER,
          unique: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    );

    setTimeout(function() {

      migration.migrator.sequelize.query('ALTER TABLE "oAuth" ADD CONSTRAINT "applicationId" FOREIGN KEY ("applicationId") REFERENCES "applications" ("id") ON DELETE CASCADE ON UPDATE CASCADE;');


      migration.migrator.sequelize.query('ALTER TABLE "authGitHub" ADD CONSTRAINT "userId" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;');

      done();
    }, 10);
  },
  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.migrator.sequelize.query('ALTER TABLE "tokens" DROP CONSTRAINT IF EXISTS tokens_pkey');
    migration.migrator.sequelize.query('ALTER TABLE "tokens" ADD CONSTRAINT tokens_pkey PRIMARY KEY("userId")');
    migration.dropTable('oAuth');
    migration.dropTable('applications');
    migration.dropTable('authGitHub');
    done();
  }
}
