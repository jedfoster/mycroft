module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.migrator.sequelize.query('ALTER TABLE "tokens" ADD CONSTRAINT "userId" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE');

    done();
  },
  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.migrator.sequelize.query('ALTER TABLE ONLY "tokens" DROP CONSTRAINT "userId"');

    done();
  }
}
