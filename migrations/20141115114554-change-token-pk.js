module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.migrator.sequelize.query('ALTER TABLE "tokens" DROP CONSTRAINT IF EXISTS tokens_pkey');
    migration.migrator.sequelize.query('ALTER TABLE "tokens" ADD CONSTRAINT tokens_pkey PRIMARY KEY("token", "userId")');

    done();
  },
  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.migrator.sequelize.query('ALTER TABLE "tokens" DROP CONSTRAINT IF EXISTS tokens_pkey');
    migration.migrator.sequelize.query('ALTER TABLE "tokens" ADD CONSTRAINT tokens_pkey PRIMARY KEY("token")');

    done();
  }
}

