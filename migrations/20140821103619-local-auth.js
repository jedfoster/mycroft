module.exports = {
  up: function(migration, DataTypes, done) {
    debugger;

    // add altering commands here, calling 'done' when finished
    migration.createTable(
      'localAuth',
      {
        userId: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false
        },

        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    );

    done();
  },
  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.dropTable('localAuth').complete(done);
  }
}
