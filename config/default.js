var dbConfig = require('./database.json'),
    ghConfig = require('./github.json');

module.exports = {
  // Customter module configs
  database: {
    development: dbConfig.development,
    test: dbConfig.test,
    production: dbConfig.production
  },
  bcrypt: {
    development: 2,
    test: 1,
    production: 12
  },
  github: {
    id: process.env.GITHUB_ID || ghConfig.id,
    secret: process.env.GITHUB_SECRET || ghConfig.secret
  }
}

