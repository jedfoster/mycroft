var bcrypt = require('bcrypt'),
    crypto = require('crypto'),
    app = require(__dirname + '/../');

module.exports = function(sequelize, DataTypes) {
  var Token = sequelize.define('Token', {
    token: {
      type: DataTypes.STRING,
      primaryKey: true,
      set: function(input) {
        input = this.getDataValue('userId') + '.' + Date.now() + '.' + Math.random();
        // No need to go crazy with the work factor, just need a random hash 
        input = bcrypt.hashSync(input, 1);
        input = crypto.createHash('sha1').update(input).digest("hex");

        return this.setDataValue('token', input);
      }
    },
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      references: 'Users',
      referencesKey: 'id'
    },
    expires: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      set: function(input) {
        input.setDate(input.getDate() + 90);

        return this.setDataValue('expires', input);
      }
    }
  }, {
    tableName: 'tokens',
    associate: function(models) {
      Token.belongsTo(models.Users);
    }
  });

  return Token
}

