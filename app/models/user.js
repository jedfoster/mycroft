var _ = require('lodash');
var validator = require('validator').validators;
var app = require(__dirname + '/../');

module.exports = function(sequelize, DataTypes) {

  var User = sequelize.define('User', {
      name: DataTypes.STRING,
      username: {
        type: DataTypes.STRING,
        unique: true
      },
      email: {
        type: DataTypes.STRING,
        unique: true
      },
      gravatarId: DataTypes.STRING
    }, {
      tableName: 'users',
      associate: function(models) {
        User.hasOne(models.AuthLocal);
        User.hasOne(models.Token);
        User.hasOne(models.AuthGitHub);
      },
      onDelete: 'cascade',
      classMethods: {
        get: function(key, fn) {
          var where;
          var include = [];

          if(validator.isInt(key)) {
            where = { id: key }
          }
          else if(validator.isEmail(key)) {
            where = { email: key };
          }
          else {
            where = { username: key };
          }

          _.each(this.associations, function(value, key) {
            include.push(app.get('models')[value.target.name])
          });

          User.find({where: where, include: include})
            .error(function(err) {
              fn(err);
            })
            .success(function(user) {
              fn(user);
            });
        },
        login: function(username, password, done) {
          this.get(username, function(user) {
            if(! user) {
              return done(false, null);
            }

            else if(user.authLocal.validPassword(password)) {
              return user.createToken({userId: user.id, token: null})
                .error(function(err) {
                  done(err, null);
                })
                .success(function(token) {
                  _.extend(user.dataValues, {token: token.token});

                  return done(null, user);
                });
            }

            return done(false, null);
          });
        },
        verifyToken: function(id, token, done) {
          this.get(id, function(user) {
            if(! user) {
              return done(false);
            }

            else {
              user.getToken()
                .success(function(userToken) {
                  if(token !== userToken.token) {
                    return done(false);
                  }
                  else {
                    return done(user);
                  }
                });
            }
          });
        }
      },
      instanceMethods: {
        destroyTokens: function(done) {
          app.get('models').Token.destroy({userId: this.id})
            .complete(done)
        },
        toJSON: function () {
          var json = this.values;
          delete json['authLocal'];
          delete json['authGitHub'];
          delete json['email'];
          delete json['createdAt'];
          delete json['updatedAt'];

          if(_.isEmpty(json['tokens'])) {
            delete json['tokens'];
          }

          return json;
        }
      }
  });

  // Save AuthLocal too
  User.afterCreate(function(user, fn) {
    if(! user.selectedValues.password) {
      return fn(null, user);
    }

    var auth = _.extend({userId: user.id}, user.selectedValues);

    user.createAuthLocal(auth)
      .error(function(err) {
        fn(err);
      })
      .success(function(user) {
        fn(null, user);
      });
  });

  return User;
}

