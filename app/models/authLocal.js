var bcrypt = require('bcrypt'),
    app = require(__dirname + '/../'),
    config = require('config').bcrypt[app.env];

module.exports = function(sequelize, DataTypes) {
  var AuthLocal = sequelize.define('AuthLocal', {
    password: {
      type: DataTypes.STRING,
      set: function(input) {
        return this.setDataValue('password', bcrypt.hashSync(input, config));
      }
    },
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      references: 'Users',
      referencesKey: 'id'
    }
  }, {
    tableName: 'authLocal',
    associate: function(models) {
      AuthLocal.belongsTo(models.Users);
    },
    instanceMethods: {
      validPassword: function(password) {
        return bcrypt.compareSync(password, this.getDataValue('password'))
      }
    }
  });

  return AuthLocal;
}

