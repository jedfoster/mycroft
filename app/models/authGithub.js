var app = require(__dirname + '/../');

module.exports = function(sequelize, DataTypes) {
  var AuthGitHub = sequelize.define('AuthGitHub', {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      references: 'Users',
      referencesKey: 'id'
    },
    githubUserId: {
      type: DataTypes.INTEGER
    }
  }, {
    tableName: 'authGitHub',
    associate: function(models) {
      AuthGitHub.belongsTo(models.Users, { foreignKey: 'userId' });
    }
  });

  return AuthGitHub
}

