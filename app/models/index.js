var Sequelize = require('sequelize'),
    app       = require(__dirname + '/../'),
    config    = require('config').database[app.env],
    sequelize = new Sequelize(config.database, config.username, config.password, config);

// load models
var models = [
  'User',
  'AuthLocal',
  'Token',
  'AuthGitHub'
];
models.forEach(function(model) {
  module.exports[model] = sequelize.import(__dirname + '/' + model);
});

// describe relationships
(function(m) {
  m.User.hasOne(m.AuthLocal).hasOne(m.Token).hasOne(m.AuthGitHub);
  m.AuthLocal.belongsTo(m.User, { foreignKey: 'userId' });
  m.Token.belongsTo(m.User, { foreignKey: 'userId' });
  m.AuthGitHub.belongsTo(m.User, { foreignKey: 'userId' });
})(module.exports);

// export connection
module.exports.sequelize = sequelize;

