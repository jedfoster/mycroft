// set variables for environment
var express = require('express');
var app = module.exports = express();
var logger = require('morgan');
var bodyParser = require('body-parser');
var github = require('config').github,
    GitHubStrategy = require('passport-github').Strategy,
    _ = require('lodash');

var passport = app.passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;

var form = require('express-form'),
    field = form.field;

form.configure({
  autoTrim: true
});

app.env = process.env.NODE_ENV || 'development';

app.use(logger('dev'));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(express.static(__dirname + '/../public'));

app.use(bodyParser.json());

app.use(passport.initialize());

app.set('models', require('./models'));

var controllers = require('./controllers');


// Respond to requests
if(app.env == 'development') {
  app.get('/', controllers.development.index);
}

var apiPath = '/api/v1',
    apiController = controllers.api.v1;


passport.use(new LocalStrategy(
  function(username, password, done) {
    return app.get('models').User.login(username, password, done);
  }
));

passport.use(new GitHubStrategy({
    clientID: github.id,
    clientSecret: github.secret,
    callbackURL: apiPath + "/github"
  },
  function(accessToken, refreshToken, profile, done) {
    var User = app.get('models').User,
        AuthGitHub = app.get('models').AuthGitHub;

    User.find({
      where: { 'authGitHub.githubUserId': profile.id },
      include: [{ model: AuthGitHub, as: AuthGitHub.tableName }]
    })
      .error(function(err) {
        console.log('ERROR: ', err);
      })
      .success(function(user) {
        console.log(user);
        if(! user) {
          User.create({
            name: profile.displayName,
            username: profile.username,
            email: profile.emails[0].value
          })
            .error(function(err) {
              console.log('ERROR: ', 'There was an error creating User');
              return done(err);
            })
            .success(function(user) {
              user.createAuthGitHub({userId: user.id, githubUserId: profile.id})
                .error(function(err) {
                  console.log('ERROR: ', 'There was an error linking User to GitHub.')
                  return done(err);
                })
                .success(function(ghUser) {
                  _.extend(user.dataValues, {token: req.user.accessToken})

                  return done(null, user);
                });
            });
        }
        else {
          _.extend(user.dataValues, {token: accessToken})

          return done(null, user);
        }
      });
  }
));


var createValidations = function() {
  return form(
    field('name').required(),
    field('username').required().is(/[a-z0-9_]+/),
    field('email').required().isEmail(),
    field('password').required(),
    field('passwordConfirm').equals('field::password')
   );
};

var updateValidations = function() {
  return form(
    field('id').required().isInt(),
    field('name'),
    field('username').is(/[a-z0-9_]+/),
    field('email').isEmail(),
    field('password'),
    field('passwordConfirm').equals('field::password')
  );
};

var deleteValidations = function() {
  return form(
    field('id').required().isInt()
  );
};

var loginValidations = function() {
  return form(
    field('username').required(),
    field('password').required()
  );
};

var authorizationValidations = function() {
  return form(
    field('id').required().isInt(),
    field('token').required()
  );
};


app.post(apiPath + '/create', createValidations(), apiController.create);
app.patch(apiPath + '/update/:id', updateValidations(), apiController.update);
app.delete(apiPath + '/delete/:id', deleteValidations(), apiController.delete);
app.post(apiPath + '/login', loginValidations(), apiController.authenticateLocal);
app.head(apiPath + '/authorization', authorizationValidations(), apiController.authorization);
app.post(apiPath + '/logout', authorizationValidations(), apiController.logout);

// This app is a _micro-service_, so none of these routes are accessed or accessible by the world.
// The host app acts as a proxy, passing requests and headers through to this app. So in the case of the GitHub authentication flow, the process should look something like this:
// 
//   1. GET host.app/login proxies to mycroft.app/api/v1/github
//   2. mycroft.app/v1/github issues a redirect to GitHub with the necessary parameters (client_id, etc.)
//   3. Once the user grants access in the GitHub UI, GitHub redirects back to host.app/<callbackURL>
//   4. GET host.app/<callbackURL> proxies to mycroft.app/api/v1/github
//   5. mycroft.app/api/v1/github then handles updating the DB, etc. and returns a JSON object with user info, including a GH access token
//   6. host.app/<callbackURL> sets cookies to persist the logged in state and redirects to the main endpoint for host.app (usually '/')

app.get(apiPath + '/github', apiController.authenticateGitHub);


// With the express server and routes defined, we can start to listen
// for requests. Heroku defines the port in an environment variable.
// Our app should use that if defined, otherwise 4000 is a pretty good default.
app.port = process.env.PORT || 1337;
app.listen(app.port);
console.log("The server is now listening on port %s", app.port);

