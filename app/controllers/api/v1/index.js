var app = require(__dirname + '/../../../');
var _ = require('lodash');

var User = app.get('models').User,
    AuthGitHub = app.get('models').AuthGitHub;

var authenticate = function(strategy, res) {
  return app.passport.authenticate(strategy, {session: false}, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(404).end();
    }

    res.json( user.toJSON() );

  });
};


exports.create = function(req, res) {
  if(! req.form.isValid) {
    return res.status(422).json({'errors': req.form.errors});
  }

  User.create(req.form)
    .error(function(err) {
      res.status(422).json({'errors': ['There was an error']});
    })
    .success(function(user) {
      res.json( user.toJSON() );
    });
};


exports.update = function(req, res) {
  if(! req.form.isValid) {
    return res.status(422).json({'errors': req.form.errors})
  }

  var updatedUser = _.reduce(req.form, function(result, value, key) {
    if(value != '') {
      result[key] = value;
    }

    return result;
  }, {});

  User.get(req.form.id, function(user) {
    if(_.isEmpty(updatedUser)) {
      return res.json( user.toJSON() );
    }

    user.updateAttributes(updatedUser)
      .error(function(err) {
        done(err);
      })
      .success(function(user) {
        res.json( user.toJSON() );
      });
  });
};


exports.delete = function(req, res) {
  if(! req.form.isValid) {
    return res.status(422).end()
  }

  User.get(req.form.id, function(user) {
    if(! user) {
      return res.status(404).end();
    }

    user.destroy()
      .error(function(err) {
        done();
      })
      .success(function() {
        res.status(204).end();
      });
  });
};


exports.authenticateLocal = function(req, res, next) {
  if(! req.form.isValid) {
    return res.status(422).end()
  }

  authenticate('local', res)(req, res, next);
};


exports.authenticateGitHub = function(req, res, next) {
  authenticate('github', res)(req, res, next);
};


exports.authorization = function(req, res) {
  if(! req.form.isValid) {
    return res.status(422).end();
  }
  else {
    User.verifyToken(req.form.id, req.form.token, function(validToken) {
      if(validToken) {
        return res.status(200).end();
      }

      return res.status(404).end();
    });
  }
};


exports.logout = function(req, res) {
  if(! req.form.isValid) {
    return res.status(422).end();
  }
  else {
    User.verifyToken(req.form.id, req.form.token, function(user) {
      if(user) {
        user.destroy()
          .error(function(err) {
            console.log(err);
            return res.status(500).end();
          })
          .success(function() {
            return res.status(200).end();
          });
      }
      else {
        return res.status(404).end();
      }
    });
  }
};

