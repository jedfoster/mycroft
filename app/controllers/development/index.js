var app = require(__dirname + '/../../');

var User = app.get('models').User;

exports.index = function(req, res) {
  res.render('index', {
    user: User.build({ name: 'Jed', email: 'jed@jedfoster.com'})
  });
}

