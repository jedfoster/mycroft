var app = require(__dirname + '/../');

if(app.env == 'development') {
  exports.development = require('./development');
}

exports.api = {
  v1: require('./api/v1')
};

