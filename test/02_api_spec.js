var request = require('supertest'),
    app = require(__dirname + '/../app');


describe('API', function() {
  var User = app.get('models').User,
      AuthLocal = app.get('models').AuthLocal,
      Token = app.get('models').Token;


  before(function(done) {
    // Silence Sequelize logging.
    app.get('models').sequelize.options.logging = false;

    User.destroy().complete(function() {
      AuthLocal.destroy().complete(function() {
        done();
      });
    });
  });


  require('./api/create')();
  require('./api/update')();
  require('./api/delete')();
  require('./api/login')();
  require('./api/authorization')();
  require('./api/logout')();
  require('./api/github')();
});

