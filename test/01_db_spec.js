var assert = require('assert'),
    bcrypt = require('bcrypt'),
    app = require(__dirname + '/../app'),
    config = require('config').bcrypt[app.env],
    _ = require('lodash');


describe('Database CRUD functions:', function() {
  var User = app.get('models').User;
  var AuthLocal = app.get('models').AuthLocal,
      AuthGitHub = app.get('models').AuthGitHub,
      Token = app.get('models').Token;

  before(function(done) {
    // Silence Sequelize logging.
    app.get('models').sequelize.options.logging = false;

    User.destroy().complete(function() {
      AuthLocal.destroy().complete(function() {
        AuthGitHub.destroy().complete(function() {
          done();
        });
      });
    });
  });


  it('should save a new user and their password', function(done) {
    User.create({ name: 'Jed', username: 'jedfoster', email: 'jed@jedfoster.com', password: 'foo'})
      .error(function(err) {
        done(err);
      })
      .success(function(user) {
        done();
      });
  });


  it('should retrieve a user by email, with their password', function(done) {
    User.get('jed@jedfoster.com', function(user) {
      assert(bcrypt.compareSync('foo', user.authLocal.password));
      done();
    });
  });


  it('should update a user\'s name', function(done) {
    User.get('jed@jedfoster.com', function(user) {
      user.updateAttributes({ name: 'George'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          assert.equal(user.name, 'George');
          done();
        });
    });
  });


  it('should update a user\'s password', function(done) {
    User.get('jed@jedfoster.com', function(user) {
      user.authLocal.updateAttributes({ password: 'bar'})
        .error(function(err) {
          done(err);
        })
        .success(function() {
          assert(user.authLocal.validPassword('bar'))
          done();
        });
    });
  });


  it('should delete a user and their password', function(done) {
    User.create({ name: 'Delete Me', username: 'deleteme', email: 'delete@jedfoster.com', password: 'foo'})
      .error(function(err) {
        done(err);
      })
      .success(function(user) {
        user.destroy()
          .error(function(err) {
            done(err);
          })
          .success(function() {
            User.find({where: { username: 'deleteme' } })
              .error(function(err) {
                done(err);
              })
              .success(function(user) {
                assert.equal(user, null);
                done();
              });
          });
      });
  });


  it('should create a token for a user', function(done) {
    User.get('jed@jedfoster.com', function(user) {
      user.createToken({userId: user.id, token: null})
        .error(function(err) {
          done(err);
        })
        .success(function(token) {
          user.getToken()
            .success(function(tokens) {
              assert(! _.isEmpty(tokens))
              done();
            });
        });
    });
  });


  it('should delete all tokens for a user', function(done) {
    User.get('jed@jedfoster.com', function(user) {
      user.getToken()
        .success(function(token) {
          token.destroy()
            .success(function() {
              user.getToken()
                .success(function(deletedToken) {
                  assert(_.isEmpty(deletedToken));
                  done()
                });
            });
        });
    });
  });


  it('should delete a user\'s tokens when the user is deleted', function(done) {
    User.create({ name: 'Delete Me', username: 'deleteme', email: 'delete@jedfoster.com', password: 'foo'})
      .error(function(err) {
        done(err);
      })
      .success(function(user) {
        user.createToken({userId: user.id, token: null})
          .error(function(err) {
            done(err);
          })
          .success(function(token) {
            user.destroy()
              .error(function(err) {
                done(err);
              })
              .success(function() {
                Token.find({where: { userId: user.id } })
                  .error(function(err) {
                    done(err);
                  })
                  .success(function(deletedToken) {
                    assert.equal(deletedToken, null);
                    done();
                  });
              });
          });
      });
  });

  describe('Given a GitHub user', function() {
    before(function(done) {
      User.create({ name: 'Github User', username: 'ghuser', email: 'ghuser@example.com'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          AuthGitHub.create({ userId: user.id, githubUserId: 12345})
            .error(function(err) {
              done(err);
            })
            .success(function(ghUser) {
              done();
            });
        });
    });

    it('should retrieve a User by GitHub id', function(done) {
      User.find({
        where: { 'authGitHub.githubUserId': 12345 },
        include: [{ model: AuthGitHub, as: AuthGitHub.tableName }]
      })
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          assert.equal(user.username, 'ghuser');
          done();
        });
    });
  });
});

