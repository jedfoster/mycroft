var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('PATCH /api/v1/update', function() {
    var updatableUserName = 'updatable.' + Date.now() + '@example.com',
        updatableUser;

    var sendRequest = function(id, form) {
      return request(app)
        .patch('/api/v1/update/' + id)
        .send(form)
        .set('Content-Type', 'application/json')
    };


    before(function(done) {
      // Silence Sequelize logging.
      app.get('models').User.create({ name: 'Update Me', username: 'updateme', email: updatableUserName, password: 'foo'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          updatableUser = user;
          done();
        });
    });


    describe('Input validation', function() {
      it('rejects a request with a malformed email', function(done) {
        var form = {
          email: 'mal@formed'
        };

        sendRequest(updatableUser.id, form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });


      it('rejects a request with mismatched passwords', function(done) {
        var form = {
          password: 'foo',
          passwordConfirm: 'bar'
        };

        sendRequest(updatableUser.id, form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });


      it('ignores empty/missing fields', function(done) {
        var form = {
          email: ''
        };

        sendRequest(updatableUser.id, form)
          .expect('Content-Type', /json/)
          .expect(function(res) {
            if(res.body.name != 'Update Me') throw new Error('expected "Update Me", got "' + res.body.name + '"');
          })
          .expect(200, done);
      });
    });


    describe('Successfully updates a user', function() {
      it('responds with the updated user', function(done) {
        var form = {
          name: 'Updated'
        };

        sendRequest(updatableUser.id, form)
          .expect('Content-Type', /json/)
          .expect(function(res) {
            if(res.body.name != 'Updated') throw new Error('expected "Updated", got "' + res.body.name + '"');
          })
          .expect(200, done);
      });
    });
  });

};

