var _ = require('lodash');
var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('POST /api/v1/logout', function() {
    var User = app.get('models').User,
        AuthLocal = app.get('models').AuthLocal,
        Token = app.get('models').Token,
        logoutUser, 
        accessToken;

    var sendRequest = function(form) {
      return request(app)
        .post('/api/v1/logout')
        .send(form)
        .set('Content-Type', 'application/json')
    };


    before(function(done) {
      User.create({ name: 'Logme Out', username: 'logmeout', email: 'log@me.out', password: 'password'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          logoutUser = user;

          user.createToken({userId: user.id, token: null})
            .error(function(err) {
              done(err);
            })
            .success(function(token) {
              accessToken = token.token;
              done();
            });
        });
    });


    describe('When a user logs out', function() {
      it('responds "OK"', function(done) {
        sendRequest({ id: logoutUser.id, token: accessToken })
          .expect(200, done);
      });

      it('should not authorize the user on a subsequent request', function(done) {
        request(app)
          .head('/api/v1/authorization')
          .send({ id: logoutUser.id, token: accessToken })
          .expect(404, done);
      });
    });
  });

};

