var _ = require('lodash');
var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('POST /api/v1/login', function() {
    var User = app.get('models').User,
        AuthLocal = app.get('models').AuthLocal,
        Token = app.get('models').Token,
        loginUser;

    var sendRequest = function(form) {
      return request(app)
        .post('/api/v1/login')
        .send(form)
        .set('Content-Type', 'application/json')
    };


    before(function(done) {
      User.create({ name: 'Logme In', username: 'logmein', email: 'log@me.in', password: 'password'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          loginUser = user;
          done();
        });
    });


    it('rejects a request with missing field(s)', function(done) {
      sendRequest({ username: 'username' })
        .expect(422, done);
    });


    describe('When a user supplies an unknown username', function() {
      it('responds "Not found"', function(done) {
        sendRequest({ username: 'username', password: 'password' })
          .expect(404, done);
      });
    });


    describe('When a user supplies an incorrect password', function() {
      it('responds "Not found"', function(done) {
        sendRequest({ username: 'logmein', password: 'bad password' })
          .expect(404, done);
      });
    });


    describe('When a user successfully logs in', function() {
      it('responds with the user\'s access token', function(done) {
        sendRequest({ username: 'logmein', password: 'password' })
          .expect('Content-Type', /json/)
          .expect(function(res) {
            if(_.isEmpty(res.body.token)) throw new Error('expected response to contain a token, got "' + JSON.stringify(res.body) + '"');
          })
          .expect(200, done);
      });
    });
  });

};

