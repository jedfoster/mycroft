var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('POST /api/v1/create', function() {
    var newUsername = 'user' + Date.now(),
        newEmail = newUsername + '@example.com';

    var sendRequest = function(form) {
      return request(app)
        .post('/api/v1/create')
        .send(form)
        .set('Content-Type', 'application/json')
    };


    describe('Input validation', function() {
      it('rejects a request with mismatched passwords', function(done) {
        var form = {
          name: 'Jed Foster',
          email: 'jed@jedfoster.com',
          username: 'username',
          password: 'foo',
          passwordConfirm: 'bar'
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });


      it('rejects a request with an empty password and password confirmation', function(done) {
        var form = {
          name: 'Jed Foster',
          email: 'jed@jedfoster.com',
          username: 'username',
          password: '',
          passwordConfirm: ''
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });


      it('rejects a request with an empty email', function(done) {
        var form = {
          name: 'Jed Foster',
          email: '',
          username: 'username',
          password: 'foo',
          passwordConfirm: 'foo'
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });


      it('rejects a request with an empty name', function(done) {
        var form = {
          name: '',
          email: 'jed@jedfoster.com',
          username: 'username',
          password: 'foo',
          passwordConfirm: 'foo'
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });


      it('rejects a request with a malformed email (username)', function(done) {
        var form = {
          name: 'Jed Foster',
          email: 'jed@jedfoster',
          username: 'username',
          password: 'foo',
          passwordConfirm: 'foo'
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });
    });


    describe('Successfully creates a user', function() {
      it('responds with the new user', function(done) {
        var form = {
          name: 'New User',
          email: newEmail,
          username: newUsername,
          password: 'foo',
          passwordConfirm: 'foo'
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(200, done);
      });
    });


    describe('Duplicate user', function() {
      it('responds with an error', function(done) {
        var form = {
          name: 'New User',
          email: newEmail,
          username: newUsername,
          password: 'foo',
          passwordConfirm: 'foo'
        };

        sendRequest(form)
          .expect('Content-Type', /json/)
          .expect(422, done);
      });
    });
  });

};

