var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('DELETE /api/v1/delete', function() {
    var deletableUser;

    var sendRequest = function(id) {
      return request(app)
        .delete('/api/v1/delete/' + id)
    };


    before(function(done) {
      app.get('models').User.create({ name: 'Delete Me', username: 'deleteme', email: 'deleteme.' + Date.now() + '@example.com', password: 'foo'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          deletableUser = user;
          done();
        });
    });

    
    describe('When the requested user doesn\'t exist', function() {
      it('returns "not found"', function(done) {
        sendRequest(deletableUser.id + 1)
          .expect(404, done);
      });
    });


    describe('When the requested user does exist', function() {
      it('deletes the user and returns ', function(done) {
        sendRequest(deletableUser.id)
          .expect(204, done);
      });
    });
  });

};

