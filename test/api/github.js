var _ = require('lodash');
var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('GET /api/v1/github', function() {

    var sendRequest = function() {
      return request(app)
        .get('/api/v1/github')
    };

    it('should redirect to GitHub', function(done) {
      sendRequest()
        .expect(302, done);
    });

  });
};

