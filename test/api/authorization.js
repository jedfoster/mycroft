var _ = require('lodash');
var request = require('supertest');
var app = require(__dirname + '/../../app');

module.exports = function() {

  describe('HEAD /api/v1/authorization', function() {
    var User = app.get('models').User,
        AuthLocal = app.get('models').AuthLocal,
        Token = app.get('models').Token,
        authUser, accessToken;

    var sendRequest = function(form) {
      return request(app)
        .head('/api/v1/authorization')
        .send(form)
    };


    before(function(done) {
      User.create({ name: 'Authorize Me', username: 'authme', email: 'auth@me.in', password: 'password'})
        .error(function(err) {
          done(err);
        })
        .success(function(user) {
          authUser = user;

          user.createToken({userId: user.id, token: null})
            .error(function(err) {
              done(err);
            })
            .success(function(token) {
              accessToken = token.token;
              done();
            });
        });
    });


    it('rejects a request with missing field(s)', function(done) {
      sendRequest({ id: '123' })
        .expect(422, done);
    });


    describe('When an access token is invalid', function() {
      it('responds "Not found"', function(done) {
        sendRequest({ id: authUser.id, token: '123' })
          .expect(404, done);
      });
    });


    describe('When an access token is valid', function() {
      it('responds "OK"', function(done) {
        sendRequest({ id: authUser.id, token: accessToken })
          .expect(200, done);
      });
    });
  });
};


